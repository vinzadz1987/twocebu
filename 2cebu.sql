-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2018 at 03:07 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2cebu`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE `accomodations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_rate` decimal(13,2) NOT NULL,
  `max_rate` decimal(13,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `attraction_id`, `description`, `remarks`, `min_rate`, `max_rate`, `created_at`, `updated_at`) VALUES
(1, 4, 'ROOM 1 (5 rooms available)', 'Non-aircon, Air-conditioned, Good for 2 pax', '1000.00', '1500.00', '2018-02-08 00:59:07', '2018-02-08 00:59:07'),
(2, 4, 'ROOM 2 (3 rooms available)', 'Air-conditioned, Good for 4 pax', '2500.00', '2500.00', '2018-02-08 00:59:07', '2018-02-08 00:59:07'),
(3, 4, 'ROOM 3 (4 rooms available)', 'Air-conditioned, Good for 6 pax', '3500.00', '3500.00', '2018-02-08 00:59:08', '2018-02-08 00:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `attraction_id`, `description`, `cost`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 4, 'Playground', '50.00', 'Per head', '2018-02-08 01:40:58', '2018-02-08 01:40:58'),
(2, 4, 'Bonfire Place', '0', 'Marshmallow Set with sticks – P100', '2018-02-08 01:40:58', '2018-02-08 01:40:58'),
(3, 6, 'Pool Use', '100.00', 'Per Adult', '2018-02-08 10:06:40', '2018-02-08 10:06:40'),
(4, 6, 'Pool Use', '50.00', 'Per head below 12 years old', '2018-02-08 10:06:40', '2018-02-08 10:06:40'),
(5, 7, 'Photography', '2500.00', 'For professional photography for events', '2018-02-08 10:15:57', '2018-02-08 10:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `attractions`
--

CREATE TABLE `attractions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double(8,2) NOT NULL,
  `longitude` double(8,2) NOT NULL,
  `festivities` text COLLATE utf8mb4_unicode_ci,
  `budget_range_min` decimal(8,2) NOT NULL DEFAULT '0.00',
  `budget_range_max` decimal(8,2) NOT NULL DEFAULT '0.00',
  `policy` text COLLATE utf8mb4_unicode_ci,
  `attraction_status` enum('approved','pending','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `status_remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `feature_banner` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attractions`
--

INSERT INTO `attractions` (`id`, `user_id`, `name`, `description`, `location`, `latitude`, `longitude`, `festivities`, `budget_range_min`, `budget_range_max`, `policy`, `attraction_status`, `status_remarks`, `is_featured`, `feature_banner`, `created_at`, `updated_at`) VALUES
(4, NULL, 'Curbada dela Maria', 'If you are looking to spend your pre-summer getaway somewhere you have never been before, there’s one NEW resort in Aloguinsan I know you will surely love.\r\n\r\nNestled on top of the mountainous part of Aloguinsan, Curbada dela Maria Resort recently attracted thousands of excited Cebuanos after teasing its aerial shot on Facebook. It features a big swimming pool, a kiddie pool and playground, a bonfire place, and colorful, vibrant cottages and rooms.', 'Kawasan, Aloguinsan, Cebu', 10.15, 123.58, 'Every June, they celebrate the famous \"Kinsan Festival\", named for a fish that is usually abundant in months of May to July. The town fiesta is celebrated every 23 and 24 October in honor of St. Raphael the Archangel.', '150.00', '210.00', '1. Do not disturb the wildlife!\r\n\r\nWhen snorkeling, swimming in the ocean, or trekking, for instance, it is important to minimize noise and movements, and be careful about disturbing what may be the natural habitat of wildlife.\r\n\r\n\r\n2. Quit straws, stirrers, and other single-use plastics.\r\n\r\nStraws, stirrers, and single-use plastics are one of the biggest culprits when it comes to water pollution. What makes them especially frustrating is that they\'re used only once and thrown away – but take forever to degrade.\r\n\r\n3. Resist the temptation to max out your accommodations.\r\n\r\nIt\'s time to quit trying to \"get your money\'s worth\" by hoarding all the free toiletries and keeping the air-conditioning running all day. If you\'re staying in a hotel, switch off all the lights and appliances, as you would do when you\'re saving on electricity at home. Also, be sure to tell the hotel staff not to replace your towels or change your sheets until you absolutely have to.\r\n\r\n4. Bring your trash home.\r\n\r\nIf you absolutely cannot avoid producing trash, make sure you take it with you when you leave the place, especially if your destination doesn\'t seem to have a proper waste facility.\r\n\r\n5.Choose your souvenirs wisely.\r\n\r\nWhile it is important to support the local community, travelers should be conscious of the souvenirs that they buy because not all of them are environmentally friendly. For instance, those small accessories with shell inlays are not the most eco-friendly souvenir to bring home. Getting zero-waste items such as fruits or local dishes are a much better choice, though it is important to bring a reusable souvenir so you can avoid packaging waste.', 'approved', NULL, 1, 'photos/feature-banners/kl1Bc20krBznMWiiwSSKJIiLRvHvN5NBVT1lCUic.jpeg', '2018-02-08 00:50:40', '2018-02-22 00:58:29'),
(5, NULL, 'Buwakan ni Alejandria', 'Brace yourselves, Sugboanons. After Florentino’s – the “Little Tagaytay of Cebu” that captured the attention of thousands of Cebuanos, a new attraction in Balamban is about to steal the spotlight. Introducing Buwakan ni Alejandra, Cebu’s newest pride featuring a vibrant flower garden. Surprisingly, Buwakan ni Alejandra (which is a Bisaya translation of “Alejandra’s flower garden”) had its soft opening since May 2017 but did not much get attention (until this post was published) probably because of “lack of social awareness” and the rising popularity of its neighboring attractions such as Adventure Cafe and Florentino’s. Or maybe, some explorers chose not to expose this hidden gem. Remember the Sirao Flower Farm? It went viral on social media, and the management had lost control over the irresponsible crowd on the first few weeks, leaving the farm damaged and in negative condition. Some people were just reckless and lacked consideration, just to get the perfect picture. Many were outraged, and that was the time when responsible tourism once again came to light.\r\n\r\n\r\nOperating hours: 7AM to 6PM, Mondays to Sundays – but if there’s a storm, they will close the premises to protect the flower garden.', 'Cebu Transcentral Hwy, Balamban, Cebu, Philippines', 10.43, 123.78, NULL, '140.00', '170.00', '2Cebu REMINDER: We strongly encourage all 2Cebu followers or readers to be responsible laagans. Make sure you follow and respect the garden’s rules. Protect and help protect the flowers by also reminding fellow tourists, especially when you see someone already stepping on the flowers. Be responsible of your own trash, and leave no trace behind.', 'approved', NULL, 0, NULL, '2018-02-08 09:37:56', '2018-02-08 22:05:18'),
(6, NULL, 'Sirao Flower Garden', 'The Little Amsterdam of Cebu is an 8,000-sq-meter piece of uphill land privately owned by Sps. Alfredo and Elena Sy-Chua who grew, for the longest time, celosia flowers which are harvested in time for the All Souls’ Day and Sinulog. Celosia flowers are locally known as burlas and are said to last for eight to 10 weeks.  \r\n\r\nSources say that these flowers come in yellow and red colors which are closely associated with the colors of Sto. Nino of Cebu.  Elena loves the Sto. Nino so dearly that she and her husband started to plant celosia flowers on their hillside land, through the labor of their garden caretaker. \r\n\r\n• Operating hours: 6AM to 6:30PM for all visitors; 6:30PM to 6:30AM for campers\r\n• Contact details: 0946 937 7891', 'Brgy. Sirao, Cebu City', 10.41, 123.87, NULL, '200.00', '250.00', '2Cebu REMINDER: We strongly encourage all 2Cebu followers or readers to be responsible laagans. Make sure you follow and respect the garden’s rules. Protect and help protect the flowers by also reminding fellow tourists, especially when you see someone already stepping on the flowers. Be responsible of your own trash, and leave no trace behind.', 'approved', NULL, 1, 'photos/feature-banners/HBLy0sDGP7Y3juzw6q22pcyVmvr0YK7vmOiVOTB0.jpeg', '2018-02-08 10:02:46', '2018-02-09 18:52:06'),
(7, NULL, 'Temple of Leah', 'Love conquers all. No buts, no don’ts. Simply giving, not bothering for what you can’t take back. Find out the story behind Cebu’s newest tourist attraction. As for Teodorico Adarna, owner of Temple of Leah and father of Filipino actress Ellen Adarna, this huge edifice of Roman Architecture design is his way of showing how strong his love and his ceaseless devotion to his wife, Leah. Yes, this is a temple of love from an inspired imagination.\r\n\r\nIn the Queen City of the South, a palace of Teodoro Adarna’s Queen Leah Villa Albino-Adarna was made and replicated with a big statue in the middle. After a few years of waiting, the Philippine version of Taj Mahal has finally opened to welcome visitors. The temple has an art gallery, museum, and a library where all of the favorite things her wife owned are placed. At the top, there’s a spectacular view of the city and more exceptional scenery as you walk through the temple. In this Greek methodology-inspired 7-story mansion, there are gigantic lions lying on each side of the stairs that will guide you to the door step of another jaw-dropping view. You can see a grand staircase with angels made up of brass and the Queen of the Temple in a 10-ft statue with crown and flower.\r\n\r\nVisiting hours: 24 hours, 7 days a week\r\nContact number: (032) 233-5032', 'Roosevelt St., Brgy. Busay Cebu City', 10.37, 123.87, NULL, '50.00', '170.00', '2Cebu REMINDER: We strongly encourage all 2Cebu followers or readers to be responsible laagans. Make sure you follow and respect the garden’s rules. Protect and help protect the flowers by also reminding fellow tourists, especially when you see someone already stepping on the flowers. Be responsible of your own trash, and leave no trace behind.', 'approved', NULL, 0, NULL, '2018-02-08 10:14:39', '2018-02-08 10:14:39'),
(8, NULL, 'Aguinid Falls', 'Aguinid Falls in the quiet municipality of Samboan, which is 150 kilometers away from Cebu City, is one of these natural wonders.\r\n\r\n\r\nAguinid Falls is unique because it is not a single waterfall. Rather it is a system of waterfalls that runs for a couple of kilometers along Tangbo River. Since it was promoted as an eco-tourism destination, locals and foreigners have witnessed this one-of-a-kind, multi-tiered waterfall system.\r\n\r\nWe had the chance to visit Aguinid Falls when FUNtastic Philippines—a nature, tourism, and heritage photography group of which we are a part of—invited us to take part of their Cebu South Photo Tour. After meeting the rest of the FUNtastic Philippines members at the Cebu City Hall and a brief pre-dawn stopover in Carcar, we arrived at the town of Moalboal, a South Cebu paradise renowned for its magnificent dive sites.', 'Santander - Barili - Toledo Rd, Samboan, Cebu, Philippines', 9.51, 123.30, NULL, '20.00', '190.00', '1. Do not disturb the wildlife!\r\n\r\nWhen snorkeling, swimming in the ocean, or trekking, for instance, it is important to minimize noise and movements, and be careful about disturbing what may be the natural habitat of wildlife.\r\n\r\n\r\n2. Quit straws, stirrers, and other single-use plastics.\r\n\r\nStraws, stirrers, and single-use plastics are one of the biggest culprits when it comes to water pollution. What makes them especially frustrating is that they\'re used only once and thrown away – but take forever to degrade.\r\n\r\n3. Resist the temptation to max out your accommodations.\r\n\r\nIt\'s time to quit trying to \"get your money\'s worth\" by hoarding all the free toiletries and keeping the air-conditioning running all day. If you\'re staying in a hotel, switch off all the lights and appliances, as you would do when you\'re saving on electricity at home. Also, be sure to tell the hotel staff not to replace your towels or change your sheets until you absolutely have to.\r\n\r\n4. Bring your trash home.\r\n\r\nIf you absolutely cannot avoid producing trash, make sure you take it with you when you leave the place, especially if your destination doesn\'t seem to have a proper waste facility.\r\n\r\n5.Choose your souvenirs wisely.\r\n\r\nWhile it is important to support the local community, travelers should be conscious of the souvenirs that they buy because not all of them are environmentally friendly. For instance, those small accessories with shell inlays are not the most eco-friendly souvenir to bring home. Getting zero-waste items such as fruits or local dishes are a much better choice, though it is important to bring a reusable souvenir so you can avoid packaging waste.', 'approved', NULL, 0, NULL, '2018-02-08 10:32:14', '2018-02-08 10:40:46'),
(9, NULL, 'Binalayan Falls', 'With an established dirt trail and a steady influx of visitors, Binalayan Falls, popularly known as Hidden Falls and the third of Samboan’s waterfall trilogy, is not really hidden anymore. But still has a mystical aura that continues to enthrall its visitors.\r\n\r\nThe entrance to Binalayan Falls is just a 5-minute ride away from Aguinid Falls. You can’t miss it since there is a sign right beside the highway.\r\n\r\nEntrance Fee: P20.00', 'Barangay Bonbon in Tangbo, Samboan, Cebu.', 9.53, 123.32, NULL, '20.00', '170.00', '1. Do not disturb the wildlife!\r\n\r\nWhen snorkeling, swimming in the ocean, or trekking, for instance, it is important to minimize noise and movements, and be careful about disturbing what may be the natural habitat of wildlife.\r\n\r\n\r\n2. Quit straws, stirrers, and other single-use plastics.\r\n\r\nStraws, stirrers, and single-use plastics are one of the biggest culprits when it comes to water pollution. What makes them especially frustrating is that they\'re used only once and thrown away – but take forever to degrade.\r\n\r\n3. Resist the temptation to max out your accommodations.\r\n\r\nIt\'s time to quit trying to \"get your money\'s worth\" by hoarding all the free toiletries and keeping the air-conditioning running all day. If you\'re staying in a hotel, switch off all the lights and appliances, as you would do when you\'re saving on electricity at home. Also, be sure to tell the hotel staff not to replace your towels or change your sheets until you absolutely have to.\r\n\r\n4. Bring your trash home.\r\n\r\nIf you absolutely cannot avoid producing trash, make sure you take it with you when you leave the place, especially if your destination doesn\'t seem to have a proper waste facility.\r\n\r\n5.Choose your souvenirs wisely.\r\n\r\nWhile it is important to support the local community, travelers should be conscious of the souvenirs that they buy because not all of them are environmentally friendly. For instance, those small accessories with shell inlays are not the most eco-friendly souvenir to bring home. Getting zero-waste items such as fruits or local dishes are a much better choice, though it is important to bring a reusable souvenir so you can avoid packaging waste.', 'approved', NULL, 0, NULL, '2018-02-08 11:10:17', '2018-02-08 11:13:42'),
(10, NULL, 'Bugnawan Falls', 'The Bugnawan Falls is one of the relatively-unknown waterfalls in the southern part of the island. It is also one of several natural tourist attractions in the Ginatilan- Samboan area. The name Bugnawan came from the term “bugnaw,” which means cold in Cebuano. It is called Bugnawan Falls due to its icy cold waters. The waters of the falls are cold since the trees surrounding the place stop the rays of the sun from hitting the water. Due to this, the water stays cold most of the time. \r\nThe Bugnawan Falls has a height of around twenty meters and depth of nearly three meters. It also features a wide catch basin where visitors can take a dip in. The Bugnawan Falls is actually located close to two other waterfalls in the town, Kampael Falls and Inambakan Falls.\r\n\r\nEntrance Fee : 20.00', 'Ginatilan, Cebu', 9.58, 123.36, 'On March 7, 2007 the Municipality launched the first \"Hinatdan Festival\" which they hope to celebrate every year. The festival is highlighted by colorful dancing of competing groups. The celebration is also in honor of the town\'s patron, St. Gregory the Great.', '20.00', '170.00', '1. Do not disturb the wildlife!\r\n\r\nWhen snorkeling, swimming in the ocean, or trekking, for instance, it is important to minimize noise and movements, and be careful about disturbing what may be the natural habitat of wildlife.\r\n\r\n\r\n2. Quit straws, stirrers, and other single-use plastics.\r\n\r\nStraws, stirrers, and single-use plastics are one of the biggest culprits when it comes to water pollution. What makes them especially frustrating is that they\'re used only once and thrown away – but take forever to degrade.\r\n\r\n3. Resist the temptation to max out your accommodations.\r\n\r\nIt\'s time to quit trying to \"get your money\'s worth\" by hoarding all the free toiletries and keeping the air-conditioning running all day. If you\'re staying in a hotel, switch off all the lights and appliances, as you would do when you\'re saving on electricity at home. Also, be sure to tell the hotel staff not to replace your towels or change your sheets until you absolutely have to.\r\n\r\n4. Bring your trash home.\r\n\r\nIf you absolutely cannot avoid producing trash, make sure you take it with you when you leave the place, especially if your destination doesn\'t seem to have a proper waste facility.\r\n\r\n5.Choose your souvenirs wisely.\r\n\r\nWhile it is important to support the local community, travelers should be conscious of the souvenirs that they buy because not all of them are environmentally friendly. For instance, those small accessories with shell inlays are not the most eco-friendly souvenir to bring home. Getting zero-waste items such as fruits or local dishes are a much better choice, though it is important to bring a reusable souvenir so you can avoid packaging waste.', 'approved', NULL, 0, 'photos/feature-banners/YtVRNb0uvrOKebz52i3wLOilOWNNHligotmtIfdV.jpeg', '2018-02-08 11:32:28', '2018-02-22 00:57:58'),
(11, NULL, 'Yap-Sandiego Ancestral House', 'The Yap-Sandiego Ancestral House is located at 155-Lopez Jaena corner Mabini Street, Parian District in Cebu, Philippines. It is just a few steps away from the Parian Monument which is also known as Heritage of Cebu Monument and meters away from Colon Street, the oldest street in the Philippines. Considered to be one of the oldest residential houses in the Philippines, the Yap-Sandiego Ancestral House was built sometime between 1675 and 1700. It was originally owned by a Chinese merchant named Don Juan Yap and his wife, Doña Maria Florido. They had 3 children, namely, Maria, Eleuterio, and Consolacion Yap.\r\n\r\nDuring the 1880’s, the oldest daughter, Maria Florido Yap, married a native from Obando, Bulacan, Don Mariano San Diego, who was Parian’s “Cabeza de Barangay ” (District Head) at that time.\r\n\r\nJust a few years ago (2008), the old ancestral home was turned over to Doña Maria’s great great grandson, Val Sandiego. Val himself is known in Cebu at present times as an art collector, renowned choreographer and heritage icon.\r\nUnder his care, he started to search for ways in restoring the Yap-Sandiego Ancestral House for he believes in the value of his ancestor’s home to the history and heritage of Cebu. And though there have been offers to buy the house from him, he still continues to ignore such proposals and vows never to sell this historical house in his lifetime.\r\n\r\nThe materials used for the construction of the Yap-Sandiego Ancestral House were coral stones that were glued together with egg whites just like Baclayon Church in Bohol. The roof is made of “Tisa” clay which weighs 1 kilogram in each piece. The wooden parts were made of “balayong” and “tugas” (molave) which were considered to be the hardest woods of all time.\r\n\r\nThe Yap-Sandiego Ancestral House is open to the public everyday from 9:00 am to 6:00 pm. It showcases some old and new artworks, life size statues, especially of the Sto. Niño, furniture’s made of “balayong”, “molave” and “narra “. Most old items which are preserved here came from Carcar, Cebu. The entrance fee is P50.00 which includes a well-versed tour guide by the name of Lloyd Gonzaga who has been a close friend of Val and a member of the well-known Sandiego Dance Troupe.\r\n\r\nThe house for its priceless antiquities and century-old treasures of Philippine history hold secrets that will make us understand more of our past. With the knowledge this house offers, we will begin to truly understand how we became what we are today. For more reservation and more information of the Yap-Sandiego Ancestral House, you may contact them at (+63) (32) 515-9000, 513-8000 and 253-5568.', '155 Mabini St, Cebu City, Cebu, Philippines', 10.30, 123.90, 'June 24 is the feast day of St. John the Baptist, patron saint of the Parian.', '7.00', '50.00', '2Cebu REMINDER: We strongly encourage all 2Cebu followers or readers to be responsible laagans. Make sure you follow and respect the museum’s rules. Help protect the museum by not touching the collections. Be responsible of your own trash, and leave no trace behind.', 'approved', NULL, 0, 'photos/feature-banners/jTibW2BYAfGLU8IdyDrAeGpeKQvJMCyI1z6bb0kg.jpeg', '2018-02-09 01:47:51', '2018-02-22 00:57:31'),
(16, 4, 'SDww', 'XCX', 'Upper Tanay Road, Lungsod ng Cebu, Lalawigan ng Cebu, Philippines', 10.42, 123.90, NULL, '33.00', '22.00', NULL, 'pending', NULL, 0, NULL, '2018-02-28 17:33:02', '2018-02-28 17:34:08'),
(17, 24, 'Cebu zoo', 'It’s no wonder Cebuanos will get giddy about the grand opening of the Cebu Safari in Carmen, another tourist spot that will surely attract a lot local and foreign visitors and boost local tourism especially in the town of Carmen.Cebu Safari Zoo, a 100-hectare safari owned by Cebuano pawnshop tycoon Michel J. Lhuillier, will feature lions, giraffes, camels, tigers, zebras, among others.\r\nDID YOU KNOW? Cebu Safari is more than just an animal kingdom. It is also a WORLD-CLASS adventure park. It will house:\r\n• Seven (7) adventure rides including zip coaster\r\n• ASIA’s Longest Zipline (1.2km)\r\n• Giant Swing\r\n• Obstacle Course\r\n• Spider Walk\r\n• Sky bike\r\n• Outdoor adventure + a full hour adventure trek in Carmen’s 8 waterfalls', 'Capitol Hills, Cebu City, 6000 Cebu', 10.34, 123.88, NULL, '400.00', '900.00', '2Cebu REMINDER: We strongly encourage all 2Cebu followers or readers to be responsible laagans. Make sure you follow and respect the rules. Be responsible of your own trash, and leave no trace behind.', 'approved', NULL, 0, NULL, '2018-03-01 16:41:36', '2018-03-01 16:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `attraction_categories`
--

CREATE TABLE `attraction_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_categories`
--

INSERT INTO `attraction_categories` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Flower Garden', '2018-01-09 05:17:49', '2018-01-09 05:17:49'),
(2, 'Mountains', '2018-01-09 21:51:59', '2018-01-09 21:51:59'),
(3, 'Beaches', '2018-01-09 21:52:20', '2018-01-09 21:52:20'),
(4, 'Historical Sites', '2018-01-09 21:52:37', '2018-01-09 21:52:37'),
(6, 'Hotels & Resorts', '2018-01-09 21:52:59', '2018-01-09 21:52:59'),
(7, 'Museums', '2018-01-09 21:53:17', '2018-01-09 21:53:17'),
(8, 'Waterfalls', '2018-01-09 21:53:28', '2018-01-09 21:53:28'),
(9, 'Food/Restaurants', '2018-01-09 21:53:37', '2018-01-09 21:53:37'),
(10, 'Churches', '2018-01-09 21:53:47', '2018-01-09 21:53:47'),
(14, 'Spa', '2018-02-09 18:49:29', '2018-02-09 18:49:29'),
(15, 'Zoo', '2018-03-01 16:41:36', '2018-03-01 16:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `attraction_category`
--

CREATE TABLE `attraction_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_category`
--

INSERT INTO `attraction_category` (`id`, `attraction_id`, `category_id`, `created_at`, `updated_at`) VALUES
(7, 4, 6, NULL, NULL),
(8, 5, 1, NULL, NULL),
(10, 6, 1, NULL, NULL),
(11, 7, 2, NULL, NULL),
(12, 7, 7, NULL, NULL),
(13, 8, 8, NULL, NULL),
(14, 9, 8, NULL, NULL),
(15, 10, 8, NULL, NULL),
(16, 11, 7, NULL, NULL),
(17, 16, 3, NULL, NULL),
(18, 17, 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attraction_like`
--

CREATE TABLE `attraction_like` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_like`
--

INSERT INTO `attraction_like` (`id`, `user_id`, `attraction_id`, `created_at`, `updated_at`) VALUES
(5, 8, 6, NULL, NULL),
(6, 23, 5, NULL, NULL),
(7, 23, 6, NULL, NULL),
(8, 23, 7, NULL, NULL),
(9, 23, 10, NULL, NULL),
(10, 23, 11, NULL, NULL),
(11, 23, 4, NULL, NULL),
(12, 23, 9, NULL, NULL),
(13, 24, 4, NULL, NULL),
(14, 24, 11, NULL, NULL),
(15, 24, 8, NULL, NULL),
(16, 24, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attraction_tag`
--

CREATE TABLE `attraction_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_tag`
--

INSERT INTO `attraction_tag` (`id`, `attraction_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(9, 4, 13, NULL, NULL),
(10, 4, 14, NULL, NULL),
(11, 4, 15, NULL, NULL),
(12, 4, 16, NULL, NULL),
(13, 4, 17, NULL, NULL),
(14, 4, 18, NULL, NULL),
(15, 4, 19, NULL, NULL),
(16, 5, 1, NULL, NULL),
(17, 5, 2, NULL, NULL),
(18, 5, 20, NULL, NULL),
(19, 5, 21, NULL, NULL),
(20, 6, 1, NULL, NULL),
(21, 6, 2, NULL, NULL),
(22, 6, 22, NULL, NULL),
(23, 6, 23, NULL, NULL),
(24, 6, 24, NULL, NULL),
(25, 7, 25, NULL, NULL),
(26, 7, 26, NULL, NULL),
(27, 7, 27, NULL, NULL),
(28, 7, 28, NULL, NULL),
(29, 8, 3, NULL, NULL),
(30, 8, 20, NULL, NULL),
(31, 9, 3, NULL, NULL),
(32, 9, 20, NULL, NULL),
(33, 9, 29, NULL, NULL),
(34, 9, 30, NULL, NULL),
(35, 10, 3, NULL, NULL),
(36, 10, 20, NULL, NULL),
(37, 10, 31, NULL, NULL),
(38, 11, 32, NULL, NULL),
(39, 16, 1, NULL, NULL),
(40, 17, 33, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delicacies`
--

CREATE TABLE `delicacies` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delicacies`
--

INSERT INTO `delicacies` (`id`, `attraction_id`, `description`, `cost`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 4, 'Puto Buli', '30.00', 'A local kakanin (much like the popular puto bumbong) made from buri or buri palm', '2018-02-08 01:44:56', '2018-02-08 01:45:26'),
(2, 4, 'Binisayaang Manok', '100.00', 'Perfect partner for Puto Buli is Aloguinsan’s “Binisayaang Manok,” which is cooked with “tuba” or coconut wine.', '2018-02-08 01:44:56', '2018-02-08 01:45:27'),
(3, 4, 'Salboros', '30.00', 'Another favorite made from tuba is the local bread “Salboros.” The tuba in Salbaros takes the place of yeast as the leavening or raising agent.', '2018-02-08 01:44:56', '2018-02-08 01:45:27'),
(4, 4, 'Taro Balls', '10.00', 'A grated purple yam cooked with coconut cream', '2018-02-08 01:44:56', '2018-02-08 01:45:27'),
(5, 5, 'Liempo of Balamban', '200.00', 'Pork liempo or pork belly of Balamban is prepared using a special method. The residents of Balamban add a number of herbs, like dahon sibuyas or spring onions, tanglad or lemon grass, and garlic, among others, into their liempo. All of these ingredients add to the flavor of the pork. The whole dish is cooked over charcoal. In addition to its uniquely savory taste, the meat of the liempo of Balamban is also so tender that you could swear it melts in your mouth.', '2018-02-08 09:47:04', '2018-02-08 09:49:36'),
(6, 5, 'Bingka Dawa', '25.00', 'Most Filipinos love rice cakes. In Cebu, it has a myriad of types under the collective word bingka (bibingka in Tagalog). While most are made from pilit (glutinous rice), or cassava, there’s another one, a specialty ingredient that can be more expensive: dawa (millet).', '2018-02-08 09:47:04', '2018-02-08 09:47:04'),
(7, 8, 'Palagsing (Saksak Palm)', '50.00', 'The palagsing is a sweet merienda delicacy with some similarities to the suman and budbod. It is also wrapped using leaves, which gives it an appearance similar to these two much-loved snack items. It also goes well with a cup of hot chocolate or coffee. But, the palagsing has a deeper flavor compared to the suman and budbod. It is also sweeter compared to these delicacies.', '2018-02-08 10:39:56', '2018-02-08 10:39:56'),
(8, 8, 'Torta Espesyal', '50.00', 'Samboan cookies with or without coco wine. Among the tortas, Torta de Tangbo is rumored to be the best one.', '2018-02-08 10:39:56', '2018-02-08 10:39:56'),
(9, 9, 'Palagsing (Saksak Palm)', '50.00', 'The palagsing is a sweet merienda delicacy with some similarities to the suman and budbod. It is also wrapped using leaves, which gives it an appearance similar to these two much-loved snack items. It also goes well with a cup of hot chocolate or coffee. But, the palagsing has a deeper flavor compared to the suman and budbod. It is also sweeter compared to these delicacies.', '2018-02-08 11:14:40', '2018-02-08 11:14:40'),
(10, 9, 'Torta Espesyal', '50.00', 'Samboan cookies with or without coco wine. Among the tortas, Torta de Tangbo is rumored to be the best one.', '2018-02-08 11:14:40', '2018-02-08 11:14:40'),
(11, 10, 'Palagsing', '50.00', 'The palagsing is a sweet merienda delicacy with some similarities to the suman and budbod. It is also wrapped using leaves, which gives it an appearance similar to these two much-loved snack items. It also goes well with a cup of hot chocolate or coffee. But, the palagsing has a deeper flavor compared to the suman and budbod. It is also sweeter compared to these delicacies.', '2018-02-08 11:38:36', '2018-02-08 11:38:36'),
(12, 11, 'Dried Mango', '200', 'Cebu City is a major distributor of dried mangoes in the country. In fact most of the dried mangoes produced in Cebu are exported to other countries. Since you are already here at the source, it would be a great idea to bring home this sweet delicacy that is considered to be best dried in the world.', '2018-02-09 02:08:19', '2018-02-09 02:08:19'),
(13, 11, 'Danggit', '800', 'Enjoy this popular Cebuano breakfast staple. The best place to buy danggit is at the Tabo-an Market where you can haggle for the prices, especially if you’re buying for your family and friends at home.', '2018-02-09 02:08:19', '2018-02-09 02:16:32'),
(14, 11, 'Rosquillos', '120', 'The rosquillos are flat butter cookies shaped like a donut. It is one of the delicacies visitors can bring home with them from Cebu.', '2018-02-09 02:16:32', '2018-02-09 02:16:32'),
(15, 11, 'Otap', '150', 'Eating this flaky, crisp, sugar coated biscuit may pose a challenge for beginners. But, once you get the hang of it, you may end up eating everything even before you board your flight. Similar to other Cebuano delicacies, the otap is available in supermarkets and pasalubong centers in Cebu.', '2018-02-09 02:16:32', '2018-02-09 02:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_08_163154_create_attraction_categories_table', 1),
(4, '2018_01_08_164932_create_attractions_table', 1),
(5, '2018_01_08_164940_create_accomodations_table', 1),
(6, '2018_01_08_165002_create_transportations_table', 1),
(7, '2018_01_08_165017_create_tags_table', 1),
(8, '2018_01_08_165423_create_delicacies_table', 1),
(9, '2018_01_08_170138_create_activities_table', 1),
(10, '2018_01_08_170347_create_attraction_tag_table', 1),
(11, '2018_01_08_170352_create_attraction_category_table', 1),
(12, '2018_01_08_170816_create_photos_table', 1),
(13, '2018_01_13_180527_add_user_id_column_to_attractions_table', 2),
(14, '2018_01_14_092651_create_ratings_table', 2),
(15, '2018_01_22_170559_create_provinces_table', 3),
(16, '2018_01_22_181438_add_budget_range_to_attrations_table', 3),
(17, '2018_01_22_190414_create_attraction_like_table', 3),
(18, '2018_01_24_134634_add_is_featured_column_to_attractions_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `attraction_id`, `filename`, `caption`, `created_at`, `updated_at`) VALUES
(2, 4, 'photos/attraction/ZY6MtHUYvkjZOW8e7AWM7Hu13ooBLJu8ZYnd6JWP.jpeg', '/', '2018-02-08 01:36:20', '2018-02-08 01:36:20'),
(3, 4, 'photos/attraction/FHpLkrW1XbwLLM8V1DPRjGjLm70UV4Ki0RF2sGRA.jpeg', '/', '2018-02-08 01:36:20', '2018-02-08 01:36:20'),
(4, 4, 'photos/attraction/1025r8fPX7eBffIrXJ1HZ6a5sgee2PLVBWoPmjzn.jpeg', '/', '2018-02-08 01:36:20', '2018-02-08 01:36:20'),
(5, 4, 'photos/attraction/m36JgeGDMmhKUga45WHiqJLLNyfvLn6gLIJh0ghi.jpeg', '/', '2018-02-08 01:36:20', '2018-02-08 01:36:20'),
(6, 4, 'photos/attraction/euKaGF4fPhGbypQGmGr2pkyM5Icfh3voHVK6HEL1.jpeg', '/', '2018-02-08 01:36:20', '2018-02-08 01:36:20'),
(24, 5, 'photos/attraction/Gu0YGnjbXYlSAeboFTbRChxumj2DHOUDoqyh2uDw.jpeg', '/', '2018-02-08 09:51:34', '2018-02-08 09:51:34'),
(25, 5, 'photos/attraction/dyRmBfh7MX1lfjgVU7XXTHHrSlgvTh3zk1Mxgk2m.jpeg', '/', '2018-02-08 09:51:34', '2018-02-08 09:51:34'),
(26, 5, 'photos/attraction/zvT8H9RzvIZkdXzfyfgGJGNmHO1ao9peb9N34xvj.jpeg', '/', '2018-02-08 09:51:34', '2018-02-08 09:51:34'),
(27, 5, 'photos/attraction/ififcYfHZy7qQkxDpIsZ4qlXfZo7o2oFmGOAaMbp.jpeg', '/', '2018-02-08 09:51:34', '2018-02-08 09:51:34'),
(28, 5, 'photos/attraction/UW16IqSH4rca9dfcZ3oftRveGsSNW29Fz9VXkA2s.jpeg', '/', '2018-02-08 09:51:34', '2018-02-08 09:51:34'),
(29, 6, 'photos/attraction/hrLuFNG8wRmggkILmEKp75NRrPQwq0SgL9o1591t.jpeg', '/', '2018-02-08 10:07:51', '2018-02-08 10:07:51'),
(30, 6, 'photos/attraction/3Z0AM8JS15KK7vIDMHgsFJom9nzHSKkAoGeUp5ig.jpeg', '/', '2018-02-08 10:07:51', '2018-02-08 10:07:51'),
(31, 6, 'photos/attraction/ZYtpouP4q9gkswl5GdwAQOIdEXHJUT7x6FhJ4AvX.jpeg', '/', '2018-02-08 10:07:52', '2018-02-08 10:07:52'),
(32, 6, 'photos/attraction/SUq2HOKS3M1ejP81TfokbFyHeGL5T5GmqZcr2psF.jpeg', '/', '2018-02-08 10:07:52', '2018-02-08 10:07:52'),
(33, 6, 'photos/attraction/Bl77LDfqIS2PFIENR4PGAz6Z6jjHstD9KU7STJMM.jpeg', '/', '2018-02-08 10:07:52', '2018-02-08 10:07:52'),
(34, 7, 'photos/attraction/ox7dph8vfQf732tBc77kiJ8QiJoPE6oVC3ceL7G5.jpeg', '/', '2018-02-08 10:20:43', '2018-02-08 10:20:43'),
(35, 7, 'photos/attraction/bI3GImDmDrykQrpYhsvdlHZTxqYka8bSzarpeBMV.jpeg', '/', '2018-02-08 10:20:43', '2018-02-08 10:20:43'),
(36, 7, 'photos/attraction/WYljD35TuFmxDqY0uEZ7J9GJqe28OkmnKWwS2eIY.jpeg', '/', '2018-02-08 10:20:43', '2018-02-08 10:20:43'),
(37, 7, 'photos/attraction/LLycHsdkUjOVDdMtDHPfSQhjY1XzaodC3zxRjjdT.jpeg', '/', '2018-02-08 10:20:43', '2018-02-08 10:20:43'),
(38, 7, 'photos/attraction/J6xdGZ06DIp5qW5KF6EYr2C00y5FwvTTsngDlD7J.jpeg', '/', '2018-02-08 10:20:43', '2018-02-08 10:20:43'),
(39, 8, 'photos/attraction/xYXLW9eIFgBOvKMXAdlNjCKkzH517gIhaQLUbX2N.jpeg', '/', '2018-02-08 10:35:09', '2018-02-08 10:35:09'),
(40, 8, 'photos/attraction/dakY3REVN8vrCFyoPrcNdBPSrdPu63SOdNrgncof.jpeg', '/', '2018-02-08 10:35:09', '2018-02-08 10:35:09'),
(41, 8, 'photos/attraction/x1FxfFlm8qYVfzz6ZrI9FwAUYvdg7CD4JkfRiwNW.jpeg', '/', '2018-02-08 10:35:10', '2018-02-08 10:35:10'),
(42, 8, 'photos/attraction/4DldpxoxSn641XkH2AdtvIEIP01iEQeoLfiwJFQz.jpeg', '/', '2018-02-08 10:35:10', '2018-02-08 10:35:10'),
(43, 8, 'photos/attraction/4yTIl8WKxtBHGH7UUfqiv6w9LSw10piuY6ZNHcnX.png', '/', '2018-02-08 10:35:10', '2018-02-08 10:35:10'),
(49, 9, 'photos/attraction/WqZdfSpkFn74iLKFAEsecAKbsJyxMWf7nz0mUMIc.jpeg', '/', '2018-02-08 11:16:59', '2018-02-08 11:16:59'),
(50, 9, 'photos/attraction/ZIBz5eq0GrfQJSRmRtvIgT2apZm5g9ACWqtd60Kq.jpeg', '/', '2018-02-08 11:16:59', '2018-02-08 11:16:59'),
(51, 9, 'photos/attraction/YO0RJ0WSFmMyHAI9Oo15KMvEk8MDwvYfnSZmbOKg.jpeg', '/', '2018-02-08 11:16:59', '2018-02-08 11:16:59'),
(52, 9, 'photos/attraction/olsZaZ0STutGiu0U1f1Braq40PayGWgedwz585oW.jpeg', '/', '2018-02-08 11:16:59', '2018-02-08 11:16:59'),
(53, 9, 'photos/attraction/NdnpIam6BZN2iGpU2hrNi1y28guuhgmgVcyUoFJX.jpeg', '/', '2018-02-08 11:16:59', '2018-02-08 11:16:59'),
(54, 10, 'photos/attraction/Pl61ibUCEV6CMB5BwmjByQ9346OXhkPVhgaHpAVL.jpeg', '/', '2018-02-08 11:39:46', '2018-02-08 11:39:46'),
(55, 10, 'photos/attraction/P3u8eObRDB7tdyRdW4xmQ0cljVlNkI1CpjffBeoN.jpeg', '/', '2018-02-08 11:39:46', '2018-02-08 11:39:46'),
(56, 10, 'photos/attraction/JNDSLE2cETxFLze3jYRm2UYHY54WvALPFIXaPa32.jpeg', '/', '2018-02-08 11:39:46', '2018-02-08 11:39:46'),
(57, 10, 'photos/attraction/3RC7z4RfCjpHLd5kfQIuAnLjwqpUv0Sg2IQGV4mE.jpeg', '/', '2018-02-08 11:39:46', '2018-02-08 11:39:46'),
(58, 10, 'photos/attraction/jy2xpMesS9zX9kZJ9FEXLbX9Whyp6eIfwY5O8kPV.jpeg', '/', '2018-02-08 11:39:46', '2018-02-08 11:39:46'),
(59, 11, 'photos/attraction/wMJtqrkbyrm33jEHsm6Ar3ken8fOALlW77YUgZ1X.jpeg', '/', '2018-02-09 01:55:18', '2018-02-09 01:55:18'),
(60, 11, 'photos/attraction/GH8obMAetdGi90EYgi54wkA97ohe7z0axj7LG4jZ.jpeg', '/', '2018-02-09 01:55:18', '2018-02-09 01:55:18'),
(61, 11, 'photos/attraction/qSctYLLqeQBwKKbxBzykqY9JEqo2aQUGGCI9JwDY.jpeg', '/', '2018-02-09 01:55:18', '2018-02-09 01:55:18'),
(62, 11, 'photos/attraction/TiOS9gtKVvFox3zfQpoBEJCOxUqQSrDB1F4saRUk.jpeg', '/', '2018-02-09 01:55:18', '2018-02-09 01:55:18'),
(63, 11, 'photos/attraction/yJYcp89E5nyiafEBHhnkGEfeFVxuV1u4pJlCjRI4.jpeg', '/', '2018-02-09 01:55:18', '2018-02-09 01:55:18');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` enum('north','south') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `region`, `created_at`, `updated_at`) VALUES
(1, 'Bantayan', 'north', NULL, NULL),
(2, 'Bogo City', 'north', NULL, NULL),
(3, 'Borbon', 'north', NULL, NULL),
(4, 'Carmen', 'north', NULL, NULL),
(5, 'Cordova', 'north', NULL, NULL),
(6, 'Daanbantayan', 'north', NULL, NULL),
(7, 'Dalaguete', 'north', NULL, NULL),
(8, 'Danao City', 'north', NULL, NULL),
(9, 'Medellin', 'north', NULL, NULL),
(10, 'Poro', 'north', NULL, NULL),
(11, 'San Francisco', 'north', NULL, NULL),
(12, 'San Remegio', 'north', NULL, NULL),
(13, 'Santa Fe', 'north', NULL, NULL),
(14, 'Sogod', 'north', NULL, NULL),
(15, 'Alcantara', 'south', NULL, NULL),
(16, 'Alcoy', 'south', NULL, NULL),
(17, 'Alegria', 'south', NULL, NULL),
(18, 'Aloguinsan', 'south', NULL, NULL),
(19, 'Argao', 'south', NULL, NULL),
(20, 'Asturias', 'south', NULL, NULL),
(21, 'Balamban', 'south', NULL, NULL),
(22, 'Barili', 'south', NULL, NULL),
(23, 'Boljoon', 'south', NULL, NULL),
(24, 'Carcar City', 'south', NULL, NULL),
(25, 'Ginatilan', 'south', NULL, NULL),
(26, 'Moalboal', 'south', NULL, NULL),
(27, 'Oslob', 'south', NULL, NULL),
(28, 'Pinamungahan', 'south', NULL, NULL),
(29, 'Ronda', 'south', NULL, NULL),
(30, 'Samboan', 'south', NULL, NULL),
(31, 'San Fernando', 'south', NULL, NULL),
(32, 'Santander', 'south', NULL, NULL),
(33, 'Talisay City', 'south', NULL, NULL),
(34, 'Toledo City', 'south', NULL, NULL),
(35, 'Mandaue City', NULL, NULL, NULL),
(36, 'Lapulapu City', NULL, NULL, NULL),
(37, 'Cebu City', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `rating_status` enum('approved','pending','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user_id`, `attraction_id`, `review`, `rating`, `rating_status`, `created_at`, `updated_at`) VALUES
(6, 8, 5, 'Great place!', '4', 'approved', '2018-02-09 00:19:47', '2018-02-09 18:47:20'),
(7, 4, 5, 'Nice', '5', 'approved', '2018-02-09 18:55:47', '2018-02-09 18:56:14'),
(8, 14, 5, 'Not so nice :(', '3', 'approved', '2018-02-09 19:00:39', '2018-02-09 19:01:02'),
(9, 18, 5, 'So nice', '1', 'approved', '2018-02-13 00:30:01', '2018-02-21 07:28:15'),
(10, 18, 4, 'I love it', '3', 'rejected', '2018-02-13 00:30:13', '2018-02-22 00:32:02'),
(11, 4, 5, 'amazing', '1', 'approved', '2018-02-21 07:27:55', '2018-02-21 07:28:16'),
(12, 23, 4, 'Perfect place to relax :)', '5', 'pending', '2018-03-01 16:00:07', '2018-03-01 16:00:07'),
(13, 24, 4, 'Perfect place to enjoy and relax :)', '5', 'approved', '2018-03-01 16:36:47', '2018-03-01 16:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'flower gardens', '2018-01-09 05:20:21', '2018-01-09 05:20:21'),
(2, 'balamban', '2018-01-09 05:20:21', '2018-01-09 05:20:21'),
(3, 'falls', '2018-01-09 21:49:59', '2018-01-09 21:49:59'),
(4, 'free', '2018-01-09 21:49:59', '2018-01-09 21:49:59'),
(5, 'dsds', '2018-01-14 05:41:57', '2018-01-14 05:41:57'),
(6, 'sa', '2018-01-14 23:18:14', '2018-01-14 23:18:14'),
(7, 'ssa', '2018-01-18 06:54:15', '2018-01-18 06:54:15'),
(8, 'coffee', '2018-01-26 05:36:30', '2018-01-26 05:36:30'),
(9, 'cafe', '2018-01-26 05:36:30', '2018-01-26 05:36:30'),
(10, 'gardens', '2018-02-06 01:43:17', '2018-02-06 01:43:17'),
(11, 'mountains', '2018-02-06 01:43:17', '2018-02-06 01:43:17'),
(12, 'flowers', '2018-02-06 01:43:17', '2018-02-06 01:43:17'),
(13, 'pool', '2018-02-08 00:50:40', '2018-02-08 00:50:40'),
(14, 'bonfire', '2018-02-08 00:50:40', '2018-02-08 00:50:40'),
(15, 'playground', '2018-02-08 00:50:40', '2018-02-08 00:50:40'),
(16, 'nature', '2018-02-08 01:48:25', '2018-02-08 01:48:25'),
(17, 'aloguinsan', '2018-02-08 01:48:25', '2018-02-08 01:48:25'),
(18, 'resorts in aloguinsan', '2018-02-08 01:48:25', '2018-02-08 01:48:25'),
(19, 'resorts in southern cebu', '2018-02-08 01:48:25', '2018-02-08 01:48:25'),
(20, 'south', '2018-02-08 09:37:57', '2018-02-08 09:37:57'),
(21, 'Flower Farm Balamban', '2018-02-08 09:38:44', '2018-02-08 09:38:44'),
(22, 'sirao resort', '2018-02-08 10:03:27', '2018-02-08 10:03:27'),
(23, 'sirao flower farm', '2018-02-08 10:03:27', '2018-02-08 10:03:27'),
(24, 'sirao farm', '2018-02-08 10:03:27', '2018-02-08 10:03:27'),
(25, 'Cebu Museums', '2018-02-08 10:14:39', '2018-02-08 10:14:39'),
(26, 'Temple of Leah', '2018-02-08 10:14:39', '2018-02-08 10:14:39'),
(27, 'Tourist Attractions', '2018-02-08 10:14:39', '2018-02-08 10:14:39'),
(28, 'Busay', '2018-02-08 10:14:39', '2018-02-08 10:14:39'),
(29, 'samboan', '2018-02-08 11:10:21', '2018-02-08 11:10:21'),
(30, 'waterfalls', '2018-02-08 11:10:21', '2018-02-08 11:10:21'),
(31, 'Bugnawan Falls', '2018-02-08 11:32:29', '2018-02-08 11:32:29'),
(32, 'heritage', '2018-02-09 01:47:52', '2018-02-09 01:47:52'),
(33, 'animals', '2018-03-01 16:41:36', '2018-03-01 16:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `transportations`
--

CREATE TABLE `transportations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_point` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_point` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fare` decimal(13,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transportations`
--

INSERT INTO `transportations` (`id`, `attraction_id`, `description`, `start_point`, `end_point`, `fare`, `created_at`, `updated_at`) VALUES
(1, 4, 'JRK bus or any bus with Carcar-Aloguinsan route', 'South Bus Terminal', 'Gaisano Grand Carcar', '60.00', '2018-02-08 00:54:45', '2018-02-08 00:54:45'),
(2, 4, 'Motorcycle', 'Gaisano Grand Carcar', 'Curbada dela Maria', '100.00', '2018-02-08 01:54:13', '2018-02-08 01:54:13'),
(3, 5, 'V-hire', 'Ayala Center Terminal', 'Balamban via Transcentral highway route', '120.00', '2018-02-08 09:40:11', '2018-02-08 09:40:11'),
(4, 6, 'Shuttle', 'Jollibee Lahug (beside JY Square Mall)', 'Going to Temple of Leah, Tops Lookout and/or Sirao Flower Garden', '150.00', '2018-02-08 10:05:33', '2018-02-08 10:05:33'),
(5, 6, 'Motorcycle (Habal - Habal)', 'J.Y. Square Mall', 'Sirao Flower Farm', '100.00', '2018-02-08 10:05:33', '2018-02-08 10:05:33'),
(6, 7, 'Shuttle', 'Dessert Factory in SSY Center across Watsons / JY Square Mall in Lahug', 'The shuttle goes to Temple of Leah, Tops Lookout and Lantaw.', '120.00', '2018-02-08 10:15:20', '2018-02-08 10:15:20'),
(7, 7, 'Motorcycle (Habal- Habal)', 'JY Square Mall', 'Temple of Leah', '100.00', '2018-02-08 10:18:26', '2018-02-08 10:18:26'),
(8, 8, 'Bus (Ceres)', 'South Bus Terminal bound for Bato via Barili', 'Aguinid Falls in Barangay Tangbo', '170.00', '2018-02-08 10:33:38', '2018-02-08 10:33:38'),
(9, 9, 'Bus', 'South Bus Terminal (Bound for Bato via Barili', 'Binalayan Falls in Samboan', '170.00', '2018-02-08 11:11:19', '2018-02-08 11:11:19'),
(10, 10, 'Bus', 'South Bus Terminal (Bound for Bato via Barili)', 'Ginatilan, Cebu', '170.00', '2018-02-08 11:36:12', '2018-02-08 11:36:12'),
(11, 10, 'Motorcycle (Habal - Habal)', 'Ginatilan, Cebu', 'Kampael Falls', '50.00', '2018-02-08 11:36:12', '2018-02-08 11:36:12'),
(12, 11, 'Jeep with 01K code', 'SM Cebu', 'Shamrock and take a short walk to the right, you’ll find the Parian Plaza.', '7.00', '2018-02-09 01:48:48', '2018-02-09 01:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_photo` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','standard') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'standard',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `address`, `contact_number`, `gender`, `username`, `email`, `facebook_uid`, `google_uid`, `display_photo`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dab', 'Dab', 'ewewewewe', '09282811211', 'male', 'dabdab', 'admin@gmail.com', NULL, NULL, NULL, '$2y$10$KoGVyEeOI82j4muXjQrMPuje1QeCSN9Lm.bCiltCbQzu/5OjCHcOS', 'standard', 'g1iDcyCR1tNZIvIiafa1oP4ZNU2Kk6J45JeRhOA9iiWvV9SDIeHNl9XTuEQr', '2018-01-14 05:46:53', '2018-02-22 00:21:38', '2018-02-22 00:21:38'),
(2, 'sasa', 'sasa', NULL, 'sasa', 'male', 'sasa', 'leslyjeang@yahoo.com.ph', NULL, NULL, NULL, '$2y$10$xo4tqqxMUc1YUATLTSlBAOZWeQBobOjq5ekqqWp9qNzHKASg9lEaW', 'standard', 'fzAww76ucGWz2tF6oGEfFSjDfUnyR79c7Qofkhp8x3kA2guNbFoe332jGW91', '2018-01-15 05:29:58', '2018-02-22 00:21:43', '2018-02-22 00:21:43'),
(4, 'Lesly Jean', 'Go', NULL, '09392825080', 'male', 'leslyjeang123@gmail.com', 'leslyjeang123@gmail.com', NULL, '114629337497573390318', NULL, '$2y$10$Gu6jEBRUQSvSIj5We2SJSOwwIOUhsRVWPvdXyqcLY5ou8rQNic4KK', 'admin', 'umYH0sb2xV1kWMwZvKrOFS8zjZOGnmIYPxCqjTcgR6lMLMrpKILW7IxzYEo2', '2018-01-18 06:09:34', '2018-01-24 07:39:32', NULL),
(5, 'Doggo', 'Go', NULL, '09392825080', 'female', 'doggo', 'doggo@yahoo.com', NULL, NULL, NULL, '$2y$10$UmN/KgSkh7nAvEjuCA.GHueJQLrX3/W.fAfc.vuzXEqTQ1NQUFiSy', 'standard', 'kmQgWT708Eet43kgAKELc2ivtVtC6kHRSnSbq4SOeqlExaFsE0mcL7YWsmzK', '2018-01-18 07:28:07', '2018-02-22 00:21:48', '2018-02-22 00:21:48'),
(6, 'd', 'w', 'ee', '09392825080', 'female', 'www', 'dd@gmail.com', NULL, NULL, NULL, '$2y$10$OerFPW5qMFP30IdiZzEKJe0Z0J.YRtR1DmL32eBd3ouy/y9vInhQK', 'standard', NULL, '2018-01-24 07:31:43', '2018-02-22 00:21:51', '2018-02-22 00:21:51'),
(8, 'Lesly Jean', 'Go', NULL, NULL, 'female', 'leslyjeang@yahoo.com.ph', 'leslyjeang@yahoo.com.ph', '1745921662126553', NULL, NULL, '$2y$10$fv.V1vefhF467XlPkSgBn.FY6FuUHb6BRC8NLOvIe4W9Q2ZHLlNI2', 'admin', 'Ajr5kvdDizeSa6w20i455XnnnOzDodwB9aP5QVsvUMz1ffd6pv6VQpqqYRtB', '2018-01-24 08:06:58', '2018-01-24 08:06:58', NULL),
(9, 'Doggo', 'Go', NULL, '09392825080', 'male', 'dog', 'doggo@yahoo.com', NULL, NULL, NULL, '$2y$10$qbpNjl9S1d2qw/f9TmlyYe6RI5Gx86QZp9yUT49LkcRcWMiPG6hfi', 'admin', NULL, '2018-01-24 19:49:17', '2018-02-22 00:21:59', '2018-02-22 00:21:59'),
(10, 'Chuva', 'Choo', NULL, '09123456789', 'male', 'chchoo', 'chchoo@email.com', NULL, NULL, NULL, '$2y$10$26YlqHsW8aiOggO3zgP16easENK0RODDJh.bCEJ97SkZFqTikIEiu', 'standard', NULL, '2018-01-24 23:33:24', '2018-02-22 00:22:05', '2018-02-22 00:22:05'),
(11, 'Dam', 'Ilooo', NULL, '09123456789', 'male', 'das', 'dasilooo@email.com', NULL, NULL, NULL, '$2y$10$c4cM6/4aJpHHoxu.gu3x7OU/DVyoXmKSXpjodDJF7KW8Z62qFpWo2', 'standard', 'LMiMg3KPPQza3EXOFPFxuaDJ1ZRToQ7MzNtCYQt3clgufGPys4aggBIPB6oA', '2018-01-26 05:22:28', '2018-02-01 01:43:03', '2018-02-01 01:43:03'),
(12, 'Dan', 'Gealon', NULL, '09123456789', 'male', 'djgealon', 'djgealon@email.com', NULL, NULL, NULL, '$2y$10$XUC4RIhqEZCXvPriR5rnMuHhj/YdYYlAed4yHz.nElLA896/LLEoW', 'standard', 'slNKg7zVHr9odPxoYCgAPIf27YpHZWZoSNM5qUbfM4XkEnWscCeeHzDYFq60', '2018-02-06 02:06:04', '2018-02-22 00:22:17', '2018-02-22 00:22:17'),
(13, 'Reden James', 'Remorta', NULL, '09499283908', 'male', 'cocoabutterkisses', 'rdnjmsremorta@gmail.com', NULL, NULL, NULL, '$2y$10$cp8FlHI7Rvb6KgeLNeiZRugXcBVg2vmONcJkcyUUSSQk/lNZ895F2', 'standard', 'Cwon4sMN1LIPEIzpdApJxqYWlnCr9nUGFChqgeT7Y8ph1mjdmllLfg0qvO0K', '2018-02-06 04:13:11', '2018-02-06 04:13:11', NULL),
(14, 'Diana Mae', 'Velayo', NULL, '09151095622', 'female', 'dianamae_baby@yahoo.com', 'dianamvelayo@gmail.com', '1862427290435271', NULL, NULL, '$2y$10$L6O.TYM9/y9oSWo30S9oBeCqTwmsDN4Jm9Qan8RzXBRSN4..meFiG', 'standard', NULL, '2018-02-06 04:56:11', '2018-02-06 05:00:58', NULL),
(15, 'mario', 'espinosa', NULL, '09287130991', 'male', 'mario', 'mario@gmail.com', NULL, NULL, NULL, '$2y$10$d7FiJiLAFrqEqbbmZN5a8ubgTWPOqrr/lpWFgJUMRNznuvXpKvHG.', 'standard', 'v77PTjSo0tMttgOTDVV9qaRH34bwd08OuV6BS1wIS8uK3ql4FhafNKmFtA35', '2018-02-07 20:57:16', '2018-02-22 00:22:27', '2018-02-22 00:22:27'),
(16, 'Lesly Jean', 'Go', NULL, '09771596302', 'female', 'leslyjeang', 'leslyjeang123@yahoo.com', NULL, NULL, NULL, '$2y$10$aoHa0X5nThVSLGhp9nL6w.C.geqqjYK7xtlJza8O7jwe0zSM/IdAO', 'admin', 'nJxzGBnIfiYcgRlgVuTnuqO0qZG5Ii0jmQ2tcBi6rdaNyWMGM5PrNKwRg0Xc', '2018-02-07 21:08:14', '2018-02-22 00:22:52', '2018-02-22 00:22:52'),
(17, 'cole', 'sprouse', NULL, '09771596302', 'female', 'cole', 'cole@yahoo.com', NULL, NULL, NULL, '$2y$10$gVTXd9EzJqin5582BA9JK.OlJzydLtUYblRPM/P61fygI78NPQR/q', 'admin', 'Hg33A758uPZ3tg2Ks3TlD9nTbNubHmfG9GAwSZMRmTKu89tfzvx0PWhx0Pz5', '2018-02-07 21:12:35', '2018-02-22 00:22:32', '2018-02-22 00:22:32'),
(18, 'gole', 'gole', NULL, '09771596302', 'female', 'gole', 'gole2@gmail.com', NULL, NULL, NULL, '$2y$10$Xn56BzVnTfLS1ze9rldUTOLsmBzekPj83TUXiV4R7.i8JQfyHOHei', 'standard', 'IpYDUkObr8QORjbl88oW7n4GsVUfz8RhkbdgmaEtV2IKSmAQT7ywOOxoVSLx', '2018-02-13 00:29:47', '2018-02-22 00:22:37', '2018-02-22 00:22:37'),
(19, 'Dan', 'Gealon', NULL, NULL, NULL, '102607896710194314618', 'gealond@gmail.com', NULL, '102607896710194314618', NULL, '$2y$10$dBwVMsz2zx04Q/C4Q/5CLOo..DtG1gQRHENSuTPyqzkzf0y3T2HA6', 'standard', 'jv4xwdmkVa1TTyrQRGjTjUN8fqg6mVyQJ1hYCukzIwowRbzkdCFNaYomKiTW', '2018-02-22 00:04:41', '2018-02-22 00:04:41', NULL),
(20, 'Nikki', 'Maloloy-on', 'Lapu-lapu City, Cebu', '09123456789', 'female', 'nikkimaloy', 'nikkimaloy@gmail.com', NULL, NULL, NULL, '$2y$10$pi237O2ErmqY9t1bi0rUXOEhakhXJLiCyiY5d2fCWoYh75lT839f.', 'standard', NULL, '2018-02-22 00:24:53', '2018-02-22 00:24:53', NULL),
(21, 'Mario', 'Espinosa', 'Basak, Mandaue City, Cebu', '09392825080', 'male', 'marioespinosa', 'marioespinosa@gmail.com', NULL, NULL, NULL, '$2y$10$eyojnZvRRIeWWjPf.xS3gOqRHefkrjEmNQ2UyRFNgd0fd2AZxsTJ6', 'standard', NULL, '2018-02-22 00:27:20', '2018-02-22 00:27:20', NULL),
(22, 'Al', 'Moralde', 'Cagayan de Oro', '09123456789', 'male', 'almoralde', 'almoralde@gmail.com', NULL, NULL, NULL, '$2y$10$dKrooS05ccdQvh6tSliLt.GhbaMJhv8gK2VxIo0MqZtlLmjaDrlQC', 'standard', NULL, '2018-02-22 00:29:17', '2018-02-22 00:29:17', NULL),
(23, 'Benjie', 'Ben', NULL, '09123456789', 'male', 'ben10', 'ben@gmail.com', NULL, NULL, NULL, '$2y$10$P.vgiZPVrFZ3KzB87onLle.JnLb4z4Dx3UuGeOIxefLo60CMCyKqa', 'standard', '21XNPVYMCdUJFl8LL8UgZPMfL61n2xjn0r75RJZlOuGUEJS5CX0ZxNKEQxKe', '2018-03-01 15:59:14', '2018-03-01 15:59:14', NULL),
(24, 'Carleen', 'Andrade', NULL, '09123456789', 'female', 'carleen123', 'carleen@gmail.com', NULL, NULL, NULL, '$2y$10$wB86g1XmPJ/PRgMpci3Yv.CFjIHl67qCjmSxFTNGoSaP5JsG4iKF2', 'standard', NULL, '2018-03-01 16:36:01', '2018-03-01 16:36:01', NULL),
(26, 'admin', 'admin', NULL, '09123456789', 'female', 'admin', 'admin@gmail.com', NULL, NULL, NULL, '$2y$10$DThBwqSyw30O3acI97QGR.oeTfLMZSJEXVbVthj74IG7FTxvVK8lS', 'admin', 'BPskKxLv7rAZEB4gB1ldlFNYW1i3gjjl7zyfrsd2LD0Iqj358rqsw4vSvTOl', '2018-03-02 23:57:04', '2018-03-02 23:57:04', NULL),
(27, 'user', 'user', NULL, '09123456789', 'female', 'user', 'user@gmail.com', NULL, NULL, NULL, '$2y$10$rI7skwfRY3YeAfbpGbk1rOBrbYr93Hy91Vevkncck8uKT78ZlhoVe', 'standard', 'q7VCTm0vZ6X9WhThh7KOeSvPP8NngldsxoKtVZZ5Pb4fB3UbxBFgC9k6R93S', '2018-03-05 23:32:15', '2018-03-05 23:32:15', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodations`
--
ALTER TABLE `accomodations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accomodations_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activities_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `attractions`
--
ALTER TABLE `attractions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attractions_user_id_foreign` (`user_id`);

--
-- Indexes for table `attraction_categories`
--
ALTER TABLE `attraction_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attraction_category`
--
ALTER TABLE `attraction_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attraction_category_attraction_id_foreign` (`attraction_id`),
  ADD KEY `attraction_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `attraction_like`
--
ALTER TABLE `attraction_like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attraction_like_user_id_foreign` (`user_id`),
  ADD KEY `attraction_like_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `attraction_tag`
--
ALTER TABLE `attraction_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attraction_tag_attraction_id_foreign` (`attraction_id`),
  ADD KEY `attraction_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `delicacies`
--
ALTER TABLE `delicacies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delicacies_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transportations`
--
ALTER TABLE `transportations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transportations_attraction_id_foreign` (`attraction_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_facebook_uid_unique` (`facebook_uid`),
  ADD UNIQUE KEY `users_google_uid_unique` (`google_uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodations`
--
ALTER TABLE `accomodations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attractions`
--
ALTER TABLE `attractions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `attraction_categories`
--
ALTER TABLE `attraction_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `attraction_category`
--
ALTER TABLE `attraction_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `attraction_like`
--
ALTER TABLE `attraction_like`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `attraction_tag`
--
ALTER TABLE `attraction_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `delicacies`
--
ALTER TABLE `delicacies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `transportations`
--
ALTER TABLE `transportations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accomodations`
--
ALTER TABLE `accomodations`
  ADD CONSTRAINT `accomodations_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attractions`
--
ALTER TABLE `attractions`
  ADD CONSTRAINT `attractions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attraction_category`
--
ALTER TABLE `attraction_category`
  ADD CONSTRAINT `attraction_category_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attraction_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `attraction_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attraction_like`
--
ALTER TABLE `attraction_like`
  ADD CONSTRAINT `attraction_like_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attraction_like_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attraction_tag`
--
ALTER TABLE `attraction_tag`
  ADD CONSTRAINT `attraction_tag_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attraction_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delicacies`
--
ALTER TABLE `delicacies`
  ADD CONSTRAINT `delicacies_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transportations`
--
ALTER TABLE `transportations`
  ADD CONSTRAINT `transportations_attraction_id_foreign` FOREIGN KEY (`attraction_id`) REFERENCES `attractions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
